'use strict';

/**
* @ngdoc function
* @name workspaceApp.controller:QuestionsCtrl
* @description
* # QuestionsCtrl
* Controller of the workspaceApp
*/
angular.module('sdQuiz')
.controller('QuestionsCtrl', function ($scope, $timeout) {

  //Adding initial value for counter
  $scope.counter = 1800; // time limit in seconds, currently set to 30 minutes
  var stopped;
  $scope.auditor = {};
  $scope.auditor.fullname = 'Joe Innes'; // Auditor name
  $scope.auditor.email = 'joe@joeinn.es'; // Email address to send results to
  $scope.timeLimit = 100; // Set the time limit in minutes.
  $scope.questions = [{
    'question': 'Who is the best person in the world?',
    'options': ['Joe','Not Joe'],
    'correct': 'Joe'
  }, {
    'question': 'Who is the best man on the planet',
    'options': ['Joe','Not Joe'],
    'correct': 'Joe'
  }, {
    'question': 'Who is the hero that Gotham deserves?',
    'options': ['Joe','Batman'],
    'correct': 'Joe'
  }, {
    'question': 'Who was the first man to walk on the moon?',
    'options': ['Neil Armstrong','Buzz Aldrin', 'Joe Innes'],
    'correct': 'Joe Innes'
  }];

  $scope.UserDataSet = function () {
    $scope.validationPassed = true;
  };

  $scope.checkAnswers = function (questions) {
    $scope.score = 0;
    $scope.correctAnswers = 0;
    $scope.answer = 'Answer';
    $scope.correct = 'Correct';

    angular.forEach(questions, function(value, key) {
      $scope.noofquestions++;
      if ( value.answer === value.correct ) {
        $scope.correctAnswers++;
        value.yes = true;
        return 1;
      }
    });
    if ( ($scope.correctAnswers/$scope.questions.length)*100 > 0 ) {
      $scope.score = ($scope.correctAnswers/$scope.questions.length)*100;
    } else {
      $scope.score = '0'; // Hack to avoid ng-show hiding the score.
    }
  };


  $scope.currentPage = 0;
  $scope.pageSize = 5;
  $scope.numberOfPages=function(){
    return Math.ceil($scope.questions.length/$scope.pageSize);
  };

  $scope.percentComplete = $scope.currentPage / $scope.numberOfPages() * 100;
  $scope.countdown = function() {
    stopped = $timeout(function() {
      if ($scope.counter <= 0 ) {
        $scope.checkAnswers();
      }
      $scope.counter--;
      $scope.countdown();
    }, 1000);
  };
  $scope.countdown();
})

.filter('startFrom', function() {
  return function(input, start) {
    start = +start; //parse to int
    return input.slice(start);
  };
});
